from flask import Flask
from logzero import logger

app = Flask(__name__)

@app.route("/")
def hello_world():
    logger.info("In hello world")
    return {
        "messg": "hello world"
    }

if __name__ == "__main__":
    app.run(host="0.0.0.0")